package com.qa.solarsystem;

public class InvalidAOCDetailsException extends Exception {

    public InvalidAOCDetailsException(String message) {
        super(message);
    }

}
