package com.qa.solarsystem;

public class InvalidAOCException extends Exception {

    public InvalidAOCException(String message) {
        super(message);
    }

}
