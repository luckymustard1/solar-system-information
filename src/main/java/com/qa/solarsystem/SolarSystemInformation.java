package com.qa.solarsystem;

import com.qa.webservice.IWebService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Objects;
import java.util.zip.DataFormatException;

public class SolarSystemInformation {

    private String objectType, objectName;
    private String astronomicalObjectClassificationCode;
    private boolean exists;
    private BigDecimal orbitalPeriod, semiMajorAxis, radius, mass;

    private final IWebService webService;

    public SolarSystemInformation(String userID, String password, IWebService webService) {
        //Sets web service to be used (in this case a stub or mock)
        this.webService = webService;
        //Checks if the username and password is valid, then if the web service authenticates. If not, fields are set
        //to appropriate dummy values.
        if (!(validUserID(userID) && validPassword(password) && this.webService.authenticate(userID, password))) {
            //Sets fields to blank
            this.objectType = "Not allowed";
            this.objectName = "Not allowed";
            this.astronomicalObjectClassificationCode = "";
            this.exists = false;
            this.orbitalPeriod = BigDecimal.ZERO;
            this.semiMajorAxis = BigDecimal.ZERO;
            this.radius = BigDecimal.ZERO;
            this.mass = BigDecimal.ZERO;
        }
    }

    private boolean validUserID(String userID) {
        //Checks userID matches the regex for 2 capital letters, followed by 4 digits.
        //Also checks the userID doesn't have four zeroes.
        return userID.matches("[A-Z]{2}[0-9]{4}") && !userID.endsWith("0000");
    }

    private boolean validPassword(String password) {
        //Checks password matches regex requiring it to have at least one lowercase, uppercase, special and numerical
        //character, with a minimum length of 10 characters.
        return password.matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{10,}$");
    }

    public String getSemiMajorAxis() {
        //Uses function to convert big decimal to required scientific notation, and adds 'km' to the end of the String
        return convertBigDecimalToScientificNotation(semiMajorAxis) + " km";
    }

    public String getMass() {
        //Uses function to convert big decimal to required scientific notation, and adds 'kg' to the end of the String
        return convertBigDecimalToScientificNotation(mass) + " kg";
    }

    private String convertBigDecimalToScientificNotation(BigDecimal bigDecimal) {
        //Formats the bigDecimal such that the coefficient is >= 1 and < 10, and calculates ^10
        String scientificNotation = new DecimalFormat("0.##E000").format(bigDecimal);
        //By default, DecimalFormat() does not add '+' after 'E', as required in scientific format, therefore
        //if power of 10 is positive then the value is >= 1, we add the '+' manually
        if (bigDecimal.doubleValue() >= 1) {
            //Finds position of 'E' in string
            int indexOfE = scientificNotation.indexOf("E") + 1;
            //Inserts '+'
            scientificNotation = scientificNotation.substring(0, indexOfE) + "+" + scientificNotation.substring(indexOfE);
        }
        return scientificNotation;
    }

    public void initialiseAOCDetails(String astronomicalObjectClassificationCode) throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks it's a valid AOC code otherwise exception is thrown. If so, then gets details from web service
        if (setAstronomicalObjectClassificationCode(astronomicalObjectClassificationCode)) {
            String webServiceDetails = webService.getStatusInfo(astronomicalObjectClassificationCode);
            String aOCDetailsRegEx = "(S[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T),Star|A[0-9]{0,8}[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T),Asteroid|P[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T),Planet|M[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T),Moon|D[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T),DwarfPlanet|C[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T),Comet),[A-Z][A-Za-z]*,[0-9]+,[0-9]+,[0-9]+,[0-9]+";
            //Confirms returned details match regular expression for validation
            if (webServiceDetails.matches(aOCDetailsRegEx)) {
                //Splits details by ',' since they are in CSV format into array
                String[] details = webServiceDetails.split(",");
                //Each field is set from details
                setObjectType(details[1]);
                setObjectName(details[2]);
                setOrbitalPeriod(details[3]);
                setRadius(details[4]);
                setSemiMajorAxis(details[5]);
                setMass(details[6]);
                setExists(true);
            } else if (Objects.equals(webServiceDetails, "")) {
                //If nothing is returned, exception is thrown
                throw new InvalidAOCException("No such classification or SMA code");
            } else {
                //If returned AOC details are invalid, exception is thrown and exists set to false
                setExists(false);
                throw new InvalidAOCDetailsException("Invalid AOC details returned from web server.");
            }
        }
    }

    @Override
    public String toString() {
        //Overrides toString() method, using required format
        return String.format("%s, %s [%s] %s, %s", objectType, objectName, astronomicalObjectClassificationCode,
                getSemiMajorAxis(), getMass());
    }

    private void setObjectType(String objectType) throws DataFormatException {
        //Checks objectType matches required regex values, otherwise exception is thrown
        if (objectType.matches("Star|Planet|Moon|DwarfPlanet|Asteroid|Comet")) {
            this.objectType = objectType;
        } else {
            throw new DataFormatException();
        }
    }

    private void setObjectName(String objectName) throws DataFormatException {
        //Checks objectName matches required regex values, otherwise exception is thrown
        //If objectType is asteroid, then objectName has special regex to allow for possible numerical characters
        if ((objectType.matches("Asteroid") && objectName.matches("[0-9]{0,8} ?[A-Z][A-Za-z]*")) ||
                objectName.matches("[A-Z][A-Za-z]*")) {
            this.objectName = objectName;
        } else {
            throw new DataFormatException();
        }
    }

    private boolean setAstronomicalObjectClassificationCode(String astronomicalObjectClassificationCode) throws InvalidAOCException {
        //Checks AOC code matches required regex values, otherwise exception is thrown
        //Returns boolean whether AOC code was valid and set
        boolean success = false;
        if (astronomicalObjectClassificationCode
                .matches("(S|P|M|D|A[0-9]{0,8}|C)[A-Z][a-z]{2}[0-9]{1,3}(TL|M|B|L|T)")) {
            this.astronomicalObjectClassificationCode = astronomicalObjectClassificationCode;
            success = true;
        } else {
            setExists(false);
            throw new InvalidAOCException("Invalid astronomical object classification code.");
        }
        return success;
    }

    private void setExists(boolean exists) {
        this.exists = exists;
    }

    private void setOrbitalPeriod(String orbitalPeriod) {
        this.orbitalPeriod = new BigDecimal(orbitalPeriod);
    }

    private void setSemiMajorAxis(String semiMajorAxis) {
        this.semiMajorAxis = new BigDecimal(semiMajorAxis);
    }

    private void setRadius(String radius) {
        this.radius = new BigDecimal(radius);
    }

    private void setMass(String mass) {
        this.mass = new BigDecimal(mass);
    }
}
