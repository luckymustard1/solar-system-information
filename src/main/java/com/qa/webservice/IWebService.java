package com.qa.webservice;

public interface IWebService {

    boolean authenticated = false;
    
    boolean authenticate(String userID, String password);

    String getStatusInfo(String astronomicalObjectClassificationCode);

}
