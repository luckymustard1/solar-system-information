package com.qa.webservice.stub;

import com.qa.webservice.IWebService;

public class WebServiceStub implements IWebService {

    private boolean authenticated = false;

    @Override
    public boolean authenticate(String userID, String password) {
        //Always authenticates and returns true
        authenticated = true;
        return true;
    }

    @Override
    public String getStatusInfo(String astronomicalObjectClassificationCode) {
        String output = "";
        //If not authenticated, then return just an empty string
        //If authenticated, check AOC parameter and return corresponding details
        if (authenticated) {
            switch (astronomicalObjectClassificationCode) {
                case "PEar150M" :
                    output = "PEar150M,Planet,Earth,365,2440,57909050,330110000000000000000000";
                    break;
                case "DCer416M" :
                    output = "DCer416M,DwarfPlanet,Ceres,1683,470,414261000,938350000000000000000";
                    break;
                case "MPho9T" :
                    output = "MPho9T,Moon,Phobos,0,11,9376,10659000000000000";
                    break;
                default :
                    output = "No such classification or SMA code";
            }
        }
        return output;
    }
}
