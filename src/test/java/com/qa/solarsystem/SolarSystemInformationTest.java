package com.qa.solarsystem;

import com.qa.webservice.IWebService;
import com.qa.webservice.stub.WebServiceStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.zip.DataFormatException;

import static org.easymock.EasyMock.*;
import static org.junit.jupiter.api.Assertions.*;

public class SolarSystemInformationTest {

    /////////////////////////////////TESTS USING MOCKING/////////////////////////////////
    
    private IWebService mockWebService;

    @BeforeEach
    void setUp() {
        mockWebService = createMock(IWebService.class);
    }

    @Test
    void test_constructor_valid_user_id_lowest_acceptable_value_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "AA0001";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns true
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_user_id_acceptable_value_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "LF6422";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns true
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_user_id_highest_acceptable_value_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns true
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_special_character_first_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "$0Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_uppercase_character_first_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "S0Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_lowercase_character_first_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "s0Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_number_character_first_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "50Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_four_zeroes_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "AA0000";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_lowercase_chars_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "aa6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_one_lowercase_char_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "Aa6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_no_chars_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_few_digits_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG625";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_many_digits_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG625222";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_few_chars_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "G6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_many_chars_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GGA6222";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_empty_password_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_special_char_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "S0larSystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_uppercase_char_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "s0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_lowercase_char_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "S0LAR$YSTEM";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_digits_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "s0larSystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }


    @Test
    void test_constructor_invalid_password_not_enough_chars_mocking() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "s0larSyst";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_objectName_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks objectName is set to "Not allowed"
        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualResult = (String) objectName.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_objectType_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks objectType is set to "Not allowed"
        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualResult = (String) objectType.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_astronomicalObjectClassificationCode_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        String expectedResult = "";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks AOC code is set to empty string
        Field astronomicalObjectClassificationCode = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        astronomicalObjectClassificationCode.setAccessible(true);
        String actualResult = (String) astronomicalObjectClassificationCode.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_exists_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks exists is set to false
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualResult = (boolean) exists.get(cut);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_orbitalPeriod_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks orbitalPeriod is set to zero
        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_semiMajorAxis_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks semiMajorAxis is set to zero
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_mass_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks mass is set to zero
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_radius_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid userID and checks radius is set to zero
        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_objectName_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks objectName is set to "Not allowed"
        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualResult = (String) objectName.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_objectType_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks objectType is set to "Not allowed"
        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualResult = (String) objectType.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_astronomicalObjectClassificationCode_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        String expectedResult = "";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks AOC code is set to empty string
        Field astronomicalObjectClassificationCode = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        astronomicalObjectClassificationCode.setAccessible(true);
        String actualResult = (String) astronomicalObjectClassificationCode.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_exists_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks exists is set to false
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualResult = (boolean) exists.get(cut);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_orbitalPeriod_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks orbitalPeriod is set to zero
        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_semiMajorAxis_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks semiMajorAxis is set to zero
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_mass_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks mass is set to zero
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_radius_to_dummy_value_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, mockWebService);
        //Assert
        //Calls constructor in CUT with invalid password and checks radius is set to zero
        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getSemiMajorAxis_returns_exponential_form_positive_power_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getSemiMajorAxis() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(61230000000f);
        String expectedResult = "6.12E+010 km";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        semiMajorAxis.set(cut, input);
        //Act
        String actualResult = cut.getSemiMajorAxis();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getSemiMajorAxis_returns_exponential_form_zero_power_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getSemiMajorAxis() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(6.12f);
        String expectedResult = "6.12E+000 km";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        semiMajorAxis.set(cut, input);
        //Act
        String actualResult = cut.getSemiMajorAxis();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getSemiMajorAxis_returns_exponential_form_negative_power_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getSemiMajorAxis() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(0.00612f);
        String expectedResult = "6.12E-003 km";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        semiMajorAxis.set(cut, input);
        //Act
        String actualResult = cut.getSemiMajorAxis();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getMass_returns_exponential_form_positive_power_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getMass() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(61230000000f);
        String expectedResult = "6.12E+010 kg";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        mass.set(cut, input);
        //Act
        String actualResult = cut.getMass();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getMass_returns_exponential_form_zero_power_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getMass() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(6.12f);
        String expectedResult = "6.12E+000 kg";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        mass.set(cut, input);
        //Act
        String actualResult = cut.getMass();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getMass_returns_exponential_form_negative_power_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getMass() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(0.00612f);
        String expectedResult = "6.12E-003 kg";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        mass.set(cut, input);
        //Act
        String actualResult = cut.getMass();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_asteroid_mocking() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks asteroid AOC code returns correct details for an asteroid
        //Arrange
        String input = "A3Jun401M";
        String expectedResult = "A3Jun401M";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("A3Jun401M,Asteroid,SomeName,365,2440,57909050,500");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_star_mocking() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks star AOC code returns correct details
        //Arrange
        String input = "SSun27TL";
        String expectedResult = "SSun27TL";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("A3Jun401M,Asteroid,SomeName,365,2440,57909050,500");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_planet_mocking() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks planet AOC code returns correct details
        //Arrange
        String input = "PEar150M";
        String expectedResult = "PEar150M";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("A3Jun401M,Asteroid,SomeName,365,2440,57909050,500");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_moon_mocking() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks moon AOC code returns correct details
        //Arrange
        String input = "MPho9T";
        String expectedResult = "MPho9T";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("A3Jun401M,Asteroid,SomeName,365,2440,57909050,500");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_dwarf_planet_mocking() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks dwarf planet AOC code returns correct details
        //Arrange
        String input = "DCer416M";
        String expectedResult = "DCer416M";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("A3Jun401M,Asteroid,SomeName,365,2440,57909050,500");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_comet_mocking() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks comet AOC code returns correct details
        //Arrange
        String input = "CHal3B";
        String expectedResult = "CHal3B";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("A3Jun401M,Asteroid,SomeName,365,2440,57909050,500");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_invalid_planet_astronomicalObjectClassificationCode_throws_invalid_AOC_exception_mocking() {
        //Checks invalid planet AOC code throws exception
        //Arrange
        String input = "iMer58M";
        String expectedExceptionMessage = "Invalid astronomical object classification code.";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        //Act
        Exception exception = assertThrows(InvalidAOCException.class, () -> cut.initialiseAOCDetails(input));
        //Assert
        assertEquals(expectedExceptionMessage, exception.getMessage());
    }

    @Test
    void test_invalid_astronomicalObjectClassificationCode_throws_invalid_AOC_exception_missing_chars_mocking() {
        //Checks invalid AOC code throws exception
        //Arrange
        String input = "IMer5";
        String expectedExceptionMessage = "Invalid astronomical object classification code.";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        //Act
        Exception exception = assertThrows(InvalidAOCException.class, () -> cut.initialiseAOCDetails(input));
        //Assert
        assertEquals(expectedExceptionMessage, exception.getMessage());
    }

    @Test
    void test_web_service_returns_CSVs_to_initialiseAOCDetails_Earth_mocking() throws NoSuchFieldException, InvalidAOCException, IllegalAccessException, InvalidAOCDetailsException, DataFormatException {
        //Checks initialiseAOCDetails for Earth successfully returns correct details from web service's getStatusInfo()
        //Arrange
        String input = "PEar150M";
        String expectedNameResult = "Earth";
        String expectedTypeResult = "Planet";
        BigDecimal expectedOrbitalPeriodResult = new BigDecimal("365");
        BigDecimal expectedRadiusResult = new BigDecimal("2440");
        BigDecimal expectedSemiMajorAxisResult = new BigDecimal("57909050");
        BigDecimal expectedMassResult = new BigDecimal("330110000000000000000000");
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("PEar150M,Planet,Earth,365,2440,57909050,330110000000000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualExistsResult = (boolean) exists.get(cut);
        assertTrue(actualExistsResult);

        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualNameResult = (String) objectName.get(cut);
        assertEquals(expectedNameResult, actualNameResult);

        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualTypeResult = (String) objectType.get(cut);
        assertEquals(expectedTypeResult, actualTypeResult);

        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualOrbitalPeriodResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedOrbitalPeriodResult, actualOrbitalPeriodResult);

        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualRadiusResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedRadiusResult, actualRadiusResult);

        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualSemiMajorAxisResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedSemiMajorAxisResult, actualSemiMajorAxisResult);

        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualMassResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedMassResult, actualMassResult);

        verify(mockWebService);
    }

    @Test
    void test_web_service_returns_CSVs_to_initialiseAOCDetails_Ceres_mocking() throws NoSuchFieldException, InvalidAOCException, IllegalAccessException, InvalidAOCDetailsException, DataFormatException {
        //Checks initialiseAOCDetails for Ceres successfully returns correct details from web service's getStatusInfo()
        //Arrange
        String input = "DCer416M";
        String expectedNameResult = "Ceres";
        String expectedTypeResult = "DwarfPlanet";
        BigDecimal expectedOrbitalPeriodResult = new BigDecimal("1683");
        BigDecimal expectedRadiusResult = new BigDecimal("470");
        BigDecimal expectedSemiMajorAxisResult = new BigDecimal("414261000");
        BigDecimal expectedMassResult = new BigDecimal("938350000000000000000");
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("DCer416M,DwarfPlanet,Ceres,1683,470,414261000,938350000000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualExistsResult = (boolean) exists.get(cut);
        assertTrue(actualExistsResult);

        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualNameResult = (String) objectName.get(cut);
        assertEquals(expectedNameResult, actualNameResult);

        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualTypeResult = (String) objectType.get(cut);
        assertEquals(expectedTypeResult, actualTypeResult);

        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualOrbitalPeriodResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedOrbitalPeriodResult, actualOrbitalPeriodResult);

        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualRadiusResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedRadiusResult, actualRadiusResult);

        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualSemiMajorAxisResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedSemiMajorAxisResult, actualSemiMajorAxisResult);

        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualMassResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedMassResult, actualMassResult);

        verify(mockWebService);
    }

    @Test
    void test_web_service_returns_CSVs_to_initialiseAOCDetails_Phobos_mocking() throws NoSuchFieldException, InvalidAOCException, IllegalAccessException, InvalidAOCDetailsException, DataFormatException {
        //Checks initialiseAOCDetails for Phobos successfully returns correct details from web service's getStatusInfo()
        //Arrange
        String input = "MPho9T";
        String expectedNameResult = "Phobos";
        String expectedTypeResult = "Moon";
        BigDecimal expectedOrbitalPeriodResult = new BigDecimal("0");
        BigDecimal expectedRadiusResult = new BigDecimal("11");
        BigDecimal expectedSemiMajorAxisResult = new BigDecimal("9376");
        BigDecimal expectedMassResult = new BigDecimal("10659000000000000");
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("MPho9T,Moon,Phobos,0,11,9376,10659000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualExistsResult = (boolean) exists.get(cut);
        assertTrue(actualExistsResult);

        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualNameResult = (String) objectName.get(cut);
        assertEquals(expectedNameResult, actualNameResult);

        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualTypeResult = (String) objectType.get(cut);
        assertEquals(expectedTypeResult, actualTypeResult);

        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualOrbitalPeriodResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedOrbitalPeriodResult, actualOrbitalPeriodResult);

        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualRadiusResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedRadiusResult, actualRadiusResult);

        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualSemiMajorAxisResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedSemiMajorAxisResult, actualSemiMajorAxisResult);

        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualMassResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedMassResult, actualMassResult);

        verify(mockWebService);
    }

    @Test
    void test_initialiseAOCDetails_throws_exception_when_if_web_service_info_is_incorrectly_formatted_missing_AOC_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks initialiseAOCDetails throws exception of web service's getStatusInfo() returns mis-formatted information, in this case missing AOC code
        //Arrange
        String input = "PEar150M";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        String expectedExceptionMessage = "Invalid AOC details returned from web server.";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("Planet,Earth,365,2440,57909050,330110000000000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        Exception exception = assertThrows(InvalidAOCDetailsException.class, () -> cut.initialiseAOCDetails(input));
        //Assert
        assertEquals(expectedExceptionMessage, exception.getMessage());
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualResult = (boolean) exists.get(cut);
        assertFalse(actualResult);

        verify(mockWebService);
    }

    @Test
    void test_initialiseAOCDetails_throws_exception_when_AOC_does_not_exist_mocking() throws NoSuchFieldException, IllegalAccessException {
        //Checks initialiseAOCDetails for throws exception when AOC does not exist
        //Arrange
        String input = "PEat110L";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        String expectedExceptionMessage = "No such classification or SMA code";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, mockWebService);
        //Act
        Exception exception = assertThrows(InvalidAOCException.class, () -> cut.initialiseAOCDetails(input));
        //Assert
        assertEquals(expectedExceptionMessage, exception.getMessage());
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualResult = (boolean) exists.get(cut);
        assertFalse(actualResult);

        verify(mockWebService);
    }

    @Test
    void test_toString_Earth_mocking() throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks toString() for Earth successfully returns correctly formatted string of details
        //Arrange
        String input = "PEar150M";
        String expectedResult = "Planet, Earth [PEar150M] 5.79E+007 km, 3.3E+023 kg";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("PEar150M,Planet,Earth,365,2440,57909050,330110000000000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        cut.initialiseAOCDetails(input);
        //Act
        String actualResult = cut.toString();
        //Assert
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_toString_Ceres_mocking() throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks toString() for Ceres successfully returns correctly formatted string of details
        //Arrange
        String input = "DCer416M";
        String expectedResult = "DwarfPlanet, Ceres [DCer416M] 4.14E+008 km, 9.38E+020 kg";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("DCer416M,DwarfPlanet,Ceres,1683,470,414261000,938350000000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        cut.initialiseAOCDetails(input);
        //Act
        String actualResult = cut.toString();
        //Assert
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_toString_Phobos_mocking() throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks toString() for Phobos successfully returns correctly formatted string of details
        //Arrange
        String input = "MPho9T";
        String expectedResult = "Moon, Phobos [MPho9T] 9.38E+003 km, 1.07E+016 kg";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        expect(mockWebService.authenticate(userID, password)).andReturn(true);
        expectLastCall().once();
        expect(mockWebService.getStatusInfo(input)).andReturn("MPho9T,Moon,Phobos,0,11,9376,10659000000000000");
        expectLastCall().once();
        replay(mockWebService);
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        cut.initialiseAOCDetails(input);
        //Act
        String actualResult = cut.toString();
        //Assert
        assertEquals(expectedResult, actualResult);

        verify(mockWebService);
    }

    @Test
    void test_AOC_is_stored_in_correct_format_Sun_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks AOC code for the Sun is in the correct format
        //Arrange
        String input = "SSun27TL";
        String expectedResult = "SSun27TL";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        Field aOC = SolarSystemInformation.class.getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        //Act
        setAOC.invoke(cut, input);
        String actualResult = (String) aOC.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_is_stored_in_correct_format_Earth_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks AOC code for Earth is in the correct format
        //Arrange
        String input = "PEar150M";
        String expectedResult = "PEar150M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        Field aOC = SolarSystemInformation.class.getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        //Act
        setAOC.invoke(cut, input);
        String actualResult = (String) aOC.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_is_stored_in_correct_format_Asteroid_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks AOC code for an asteroid is in the correct format
        //Arrange
        String input = "A99942Apo138M";
        String expectedResult = "A99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        Field aOC = SolarSystemInformation.class.getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        //Act
        setAOC.invoke(cut, input);
        String actualResult = (String) aOC.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_setter_incorrect_format_throws_exception_mocking() throws NoSuchMethodException {
        //Checks AOC code in incorrect format throws exception
        //Arrange
        String input = "AB99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setAOC.invoke(cut, input));
    }

    @Test
    void test_AOC_setter_blank_throws_exception_mocking() throws NoSuchMethodException {
        //Checks blank AOC code (empty string) throws exception
        //Arrange
        String input = "";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setAOC.invoke(cut, input));
    }

    @Test
    void test_ObjectType_is_stored_in_correct_format_Planet_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks planet objectType is successfully stored in correct format
        //Arrange
        String input = "Planet";
        String expectedResult = "Planet";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Field objectType = SolarSystemInformation.class.getDeclaredField("objectType");
        objectType.setAccessible(true);
        //Act
        setObjectType.invoke(cut, input);
        String actualResult = (String) objectType.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectType_is_stored_in_correct_format_Moon_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks moon objectType is successfully stored in correct format
        //Arrange
        String input = "Moon";
        String expectedResult = "Moon";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Field objectType = SolarSystemInformation.class.getDeclaredField("objectType");
        objectType.setAccessible(true);
        //Act
        setObjectType.invoke(cut, input);
        String actualResult = (String) objectType.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectType_is_stored_in_correct_format_Asteroid_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks asteroid objectType is successfully stored in correct format
        //Arrange
        String input = "Asteroid";
        String expectedResult = "Asteroid";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Field objectType = SolarSystemInformation.class.getDeclaredField("objectType");
        objectType.setAccessible(true);
        //Act
        setObjectType.invoke(cut, input);
        String actualResult = (String) objectType.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectType_setter_incorrect_format_throws_exception_mocking() throws NoSuchMethodException {
        //Checks incorrectly formatted objectType throws exception
        //Arrange
        String input = "AB99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectType.invoke(cut, input));
    }

    @Test
    void test_ObjectType_setter_blank_throws_exception_mocking() throws NoSuchMethodException {
        //Checks blank objectType (empty string) throws exception
        //Arrange
        String input = "";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectType.invoke(cut, input));
    }

    @Test
    void test_ObjectName_is_stored_in_correct_format_Earth_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks objectName is correctly formatted (Earth)
        //Arrange
        String input = "Earth";
        String expectedResult = "Earth";
        String objectType = "Planet";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        Field objectName = SolarSystemInformation.class.getDeclaredField("objectName");
        objectName.setAccessible(true);
        //Act
        setObjectType.invoke(cut, objectType);
        setObjectName.invoke(cut, input);
        String actualResult = (String) objectName.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectName_is_stored_in_correct_format_Asteroid_mocking() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks objectName is correctly formatted (Asteroid)
        //Arrange
        String input = "99942 Apophis";
        String expectedResult = "99942 Apophis";
        String objectType = "Asteroid";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        Field objectName = SolarSystemInformation.class.getDeclaredField("objectName");
        objectName.setAccessible(true);
        //Act
        setObjectType.invoke(cut, objectType);
        setObjectName.invoke(cut, input);
        String actualResult = (String) objectName.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectName_setter_incorrect_format_throws_exception_mocking() throws NoSuchMethodException {
        //Checks incorrectly formatted objectName throws exception
        //Arrange
        String input = "AB99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectName.invoke(cut, input));
    }

    @Test
    void test_ObjectName_setter_blank_throws_exception_mocking() throws NoSuchMethodException {
        //Checks blank objectName (empty string) throws exception
        //Arrange
        String input = "";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", mockWebService);
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectName.invoke(cut, input));
    }

    /////////////////////////////////TESTS USING STUB/////////////////////////////////

    @Test
    void test_constructor_valid_user_id_lowest_acceptable_value_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "AA0001";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns true
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_user_id_acceptable_value_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "LF6422";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns true
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_user_id_highest_acceptable_value_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns true
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_special_character_first_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "$0Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_uppercase_character_first_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "S0Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_lowercase_character_first_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "s0Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_valid_password_number_character_first_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "ZZ9999";
        String passwordInput = "50Lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns true
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertTrue(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_four_zeroes_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "AA0000";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_lowercase_chars_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "aa6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_one_lowercase_char_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "Aa6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_no_chars_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_few_digits_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG625";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_many_digits_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG625222";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_few_chars_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "G6253";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_user_id_too_many_chars_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GGA6222";
        String passwordInput = "S0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validUserID method in CUT and checks it returns false
        Method validUserID = SolarSystemInformation.class.getDeclaredMethod("validUserID", String.class);
        validUserID.setAccessible(true);
        boolean actualResult = (boolean) validUserID.invoke(cut, userIDInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_empty_password_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_special_char_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "S0larSystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_uppercase_char_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "s0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_lowercase_char_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "S0LAR$YSTEM";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_no_digits_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "s0larSystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }


    @Test
    void test_constructor_invalid_password_not_enough_chars_stub() throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        //Arrange
        String userIDInput = "GG6222";
        String passwordInput = "s0larSyst";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls private validPassword method in CUT and checks it returns false
        Method validPassword = SolarSystemInformation.class.getDeclaredMethod("validPassword", String.class);
        validPassword.setAccessible(true);
        boolean actualResult = (boolean) validPassword.invoke(cut, passwordInput);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_objectName_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks objectName is set to "Not allowed"
        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualResult = (String) objectName.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_objectType_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks objectType is set to "Not allowed"
        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualResult = (String) objectType.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_astronomicalObjectClassificationCode_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        String expectedResult = "";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks AOC code is set to empty string
        Field astronomicalObjectClassificationCode = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        astronomicalObjectClassificationCode.setAccessible(true);
        String actualResult = (String) astronomicalObjectClassificationCode.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_exists_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks exists is set to false
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualResult = (boolean) exists.get(cut);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_orbitalPeriod_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks orbitalPeriod is set to zero
        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_semiMajorAxis_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks semiMajorAxis is set to zero
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_mass_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks mass is set to zero
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_userID_sets_radius_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GGG6222";
        String passwordInput = "s0lar$ystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid userID and checks radius is set to zero
        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_objectName_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks objectName is set to "Not allowed"
        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualResult = (String) objectName.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_objectType_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        String expectedResult = "Not allowed";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks objectType is set to "Not allowed"
        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualResult = (String) objectType.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_astronomicalObjectClassificationCode_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        String expectedResult = "";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks AOC code is set to empty string
        Field astronomicalObjectClassificationCode = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        astronomicalObjectClassificationCode.setAccessible(true);
        String actualResult = (String) astronomicalObjectClassificationCode.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_exists_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks exists is set to false
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualResult = (boolean) exists.get(cut);
        assertFalse(actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_orbitalPeriod_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks orbitalPeriod is set to zero
        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_semiMajorAxis_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks semiMajorAxis is set to zero
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_mass_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks mass is set to zero
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_constructor_invalid_password_sets_radius_to_dummy_value_stub() throws NoSuchFieldException, IllegalAccessException {
        //Arrange
        String userIDInput = "GG6111";
        String passwordInput = "s0larSystem";
        BigDecimal expectedResult = BigDecimal.ZERO;
        //Act
        SolarSystemInformation cut = new SolarSystemInformation(userIDInput, passwordInput, new WebServiceStub());
        //Assert
        //Calls constructor in CUT with invalid password and checks radius is set to zero
        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getSemiMajorAxis_returns_exponential_form_positive_power_stub() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getSemiMajorAxis() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(61230000000f);
        String expectedResult = "6.12E+010 km";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        semiMajorAxis.set(cut, input);
        //Act
        String actualResult = cut.getSemiMajorAxis();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getSemiMajorAxis_returns_exponential_form_zero_power_stub() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getSemiMajorAxis() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(6.12f);
        String expectedResult = "6.12E+000 km";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        semiMajorAxis.set(cut, input);
        //Act
        String actualResult = cut.getSemiMajorAxis();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getSemiMajorAxis_returns_exponential_form_negative_power_stub() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getSemiMajorAxis() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(0.00612f);
        String expectedResult = "6.12E-003 km";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        semiMajorAxis.set(cut, input);
        //Act
        String actualResult = cut.getSemiMajorAxis();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getMass_returns_exponential_form_positive_power_stub() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getMass() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(61230000000f);
        String expectedResult = "6.12E+010 kg";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        mass.set(cut, input);
        //Act
        String actualResult = cut.getMass();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getMass_returns_exponential_form_zero_power_stub() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getMass() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(6.12f);
        String expectedResult = "6.12E+000 kg";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        mass.set(cut, input);
        //Act
        String actualResult = cut.getMass();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_getMass_returns_exponential_form_negative_power_stub() throws NoSuchFieldException, IllegalAccessException {
        //Checks that getMass() returns value in exponential form
        //Arrange
        BigDecimal input = BigDecimal.valueOf(0.00612f);
        String expectedResult = "6.12E-003 kg";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        mass.set(cut, input);
        //Act
        String actualResult = cut.getMass();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_planet_stub() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks planet AOC code returns correct details
        //Arrange
        String input = "PEar150M";
        String expectedResult = "PEar150M";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, new WebServiceStub());
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_moon_stub() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks moon AOC code returns correct details
        //Arrange
        String input = "MPho9T";
        String expectedResult = "MPho9T";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, new WebServiceStub());
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_astronomicalObjectClassificationCode_is_valid_dwarf_planet_stub() throws NoSuchFieldException, IllegalAccessException, InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks dwarf planet AOC code returns correct details
        //Arrange
        String input = "DCer416M";
        String expectedResult = "DCer416M";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, new WebServiceStub());
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field aOC = cut.getClass().getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        String actualResult = (String) aOC.get(cut);
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_invalid_planet_astronomicalObjectClassificationCode_throws_invalid_AOC_exception_stub() {
        //Checks invalid planet AOC code throws exception
        //Arrange
        String input = "iMer58M";
        String expectedExceptionMessage = "Invalid astronomical object classification code.";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        //Act
        Exception exception = assertThrows(InvalidAOCException.class, () -> cut.initialiseAOCDetails(input));
        //Assert
        assertEquals(expectedExceptionMessage, exception.getMessage());
    }

    @Test
    void test_invalid_astronomicalObjectClassificationCode_throws_invalid_AOC_exception_missing_chars_stub() {
        //Checks invalid AOC code throws exception
        //Arrange
        String input = "IMer5";
        String expectedExceptionMessage = "Invalid astronomical object classification code.";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        //Act
        Exception exception = assertThrows(InvalidAOCException.class, () -> cut.initialiseAOCDetails(input));
        //Assert
        assertEquals(expectedExceptionMessage, exception.getMessage());
    }

    @Test
    void test_web_service_returns_CSVs_to_initialiseAOCDetails_Earth_stub() throws NoSuchFieldException, InvalidAOCException, IllegalAccessException, InvalidAOCDetailsException, DataFormatException {
        //Checks initialiseAOCDetails for Earth successfully returns correct details from web service's getStatusInfo()
        //Arrange
        String input = "PEar150M";
        String expectedNameResult = "Earth";
        String expectedTypeResult = "Planet";
        BigDecimal expectedOrbitalPeriodResult = new BigDecimal("365");
        BigDecimal expectedRadiusResult = new BigDecimal("2440");
        BigDecimal expectedSemiMajorAxisResult = new BigDecimal("57909050");
        BigDecimal expectedMassResult = new BigDecimal("330110000000000000000000");
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, new WebServiceStub());
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualExistsResult = (boolean) exists.get(cut);
        assertTrue(actualExistsResult);

        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualNameResult = (String) objectName.get(cut);
        assertEquals(expectedNameResult, actualNameResult);

        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualTypeResult = (String) objectType.get(cut);
        assertEquals(expectedTypeResult, actualTypeResult);

        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualOrbitalPeriodResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedOrbitalPeriodResult, actualOrbitalPeriodResult);

        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualRadiusResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedRadiusResult, actualRadiusResult);

        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualSemiMajorAxisResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedSemiMajorAxisResult, actualSemiMajorAxisResult);

        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualMassResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedMassResult, actualMassResult);
    }

    @Test
    void test_web_service_returns_CSVs_to_initialiseAOCDetails_Ceres_stub() throws NoSuchFieldException, InvalidAOCException, IllegalAccessException, InvalidAOCDetailsException, DataFormatException {
        //Checks initialiseAOCDetails for Ceres successfully returns correct details from web service's getStatusInfo()
        //Arrange
        String input = "DCer416M";
        String expectedNameResult = "Ceres";
        String expectedTypeResult = "DwarfPlanet";
        BigDecimal expectedOrbitalPeriodResult = new BigDecimal("1683");
        BigDecimal expectedRadiusResult = new BigDecimal("470");
        BigDecimal expectedSemiMajorAxisResult = new BigDecimal("414261000");
        BigDecimal expectedMassResult = new BigDecimal("938350000000000000000");
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, new WebServiceStub());
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualExistsResult = (boolean) exists.get(cut);
        assertTrue(actualExistsResult);

        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualNameResult = (String) objectName.get(cut);
        assertEquals(expectedNameResult, actualNameResult);

        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualTypeResult = (String) objectType.get(cut);
        assertEquals(expectedTypeResult, actualTypeResult);

        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualOrbitalPeriodResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedOrbitalPeriodResult, actualOrbitalPeriodResult);

        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualRadiusResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedRadiusResult, actualRadiusResult);

        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualSemiMajorAxisResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedSemiMajorAxisResult, actualSemiMajorAxisResult);

        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualMassResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedMassResult, actualMassResult);
    }

    @Test
    void test_web_service_returns_CSVs_to_initialiseAOCDetails_Phobos_stub() throws NoSuchFieldException, InvalidAOCException, IllegalAccessException, InvalidAOCDetailsException, DataFormatException {
        //Checks initialiseAOCDetails for Phobos successfully returns correct details from web service's getStatusInfo()
        //Arrange
        String input = "MPho9T";
        String expectedNameResult = "Phobos";
        String expectedTypeResult = "Moon";
        BigDecimal expectedOrbitalPeriodResult = new BigDecimal("0");
        BigDecimal expectedRadiusResult = new BigDecimal("11");
        BigDecimal expectedSemiMajorAxisResult = new BigDecimal("9376");
        BigDecimal expectedMassResult = new BigDecimal("10659000000000000");
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation(userID, password, new WebServiceStub());
        //Act
        cut.initialiseAOCDetails(input);
        //Assert
        Field exists = cut.getClass().getDeclaredField("exists");
        exists.setAccessible(true);
        boolean actualExistsResult = (boolean) exists.get(cut);
        assertTrue(actualExistsResult);

        Field objectName = cut.getClass().getDeclaredField("objectName");
        objectName.setAccessible(true);
        String actualNameResult = (String) objectName.get(cut);
        assertEquals(expectedNameResult, actualNameResult);

        Field objectType = cut.getClass().getDeclaredField("objectType");
        objectType.setAccessible(true);
        String actualTypeResult = (String) objectType.get(cut);
        assertEquals(expectedTypeResult, actualTypeResult);

        Field orbitalPeriod = cut.getClass().getDeclaredField("orbitalPeriod");
        orbitalPeriod.setAccessible(true);
        BigDecimal actualOrbitalPeriodResult = (BigDecimal) orbitalPeriod.get(cut);
        assertEquals(expectedOrbitalPeriodResult, actualOrbitalPeriodResult);

        Field radius = cut.getClass().getDeclaredField("radius");
        radius.setAccessible(true);
        BigDecimal actualRadiusResult = (BigDecimal) radius.get(cut);
        assertEquals(expectedRadiusResult, actualRadiusResult);

        Field semiMajorAxis = cut.getClass().getDeclaredField("semiMajorAxis");
        semiMajorAxis.setAccessible(true);
        BigDecimal actualSemiMajorAxisResult = (BigDecimal) semiMajorAxis.get(cut);
        assertEquals(expectedSemiMajorAxisResult, actualSemiMajorAxisResult);

        Field mass = cut.getClass().getDeclaredField("mass");
        mass.setAccessible(true);
        BigDecimal actualMassResult = (BigDecimal) mass.get(cut);
        assertEquals(expectedMassResult, actualMassResult);
    }

    @Test
    void test_toString_Earth_stub() throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks toString() for Earth successfully returns correctly formatted string of details
        //Arrange
        String input = "PEar150M";
        String expectedResult = "Planet, Earth [PEar150M] 5.79E+007 km, 3.3E+023 kg";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        cut.initialiseAOCDetails(input);
        //Act
        String actualResult = cut.toString();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_toString_Ceres_stub() throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks toString() for Ceres successfully returns correctly formatted string of details
        //Arrange
        String input = "DCer416M";
        String expectedResult = "DwarfPlanet, Ceres [DCer416M] 4.14E+008 km, 9.38E+020 kg";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        cut.initialiseAOCDetails(input);
        //Act
        String actualResult = cut.toString();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_toString_Phobos_stub() throws InvalidAOCException, InvalidAOCDetailsException, DataFormatException {
        //Checks toString() for Phobos successfully returns correctly formatted string of details
        //Arrange
        String input = "MPho9T";
        String expectedResult = "Moon, Phobos [MPho9T] 9.38E+003 km, 1.07E+016 kg";
        String userID = "AA0001";
        String password = "S0lar$ystem";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        cut.initialiseAOCDetails(input);
        //Act
        String actualResult = cut.toString();
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_is_stored_in_correct_format_Sun_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks AOC code for the Sun is in the correct format
        //Arrange
        String input = "SSun27TL";
        String expectedResult = "SSun27TL";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        Field aOC = SolarSystemInformation.class.getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        //Act
        setAOC.invoke(cut, input);
        String actualResult = (String) aOC.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_is_stored_in_correct_format_Earth_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks AOC code for Earth is in the correct format
        //Arrange
        String input = "PEar150M";
        String expectedResult = "PEar150M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        Field aOC = SolarSystemInformation.class.getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        //Act
        setAOC.invoke(cut, input);
        String actualResult = (String) aOC.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_is_stored_in_correct_format_Asteroid_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks AOC code for an asteroid is in the correct format
        //Arrange
        String input = "A99942Apo138M";
        String expectedResult = "A99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        Field aOC = SolarSystemInformation.class.getDeclaredField("astronomicalObjectClassificationCode");
        aOC.setAccessible(true);
        //Act
        setAOC.invoke(cut, input);
        String actualResult = (String) aOC.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_AOC_setter_incorrect_format_throws_exception_stub() throws NoSuchMethodException {
        //Checks AOC code in incorrect format throws exception
        //Arrange
        String input = "AB99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setAOC.invoke(cut, input));
    }

    @Test
    void test_AOC_setter_blank_throws_exception_stub() throws NoSuchMethodException {
        //Checks blank AOC code (empty string) throws exception
        //Arrange
        String input = "";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setAOC = SolarSystemInformation.class.getDeclaredMethod("setAstronomicalObjectClassificationCode", String.class);
        setAOC.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setAOC.invoke(cut, input));
    }

    @Test
    void test_ObjectType_is_stored_in_correct_format_Planet_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks planet objectType is successfully stored in correct format
        //Arrange
        String input = "Planet";
        String expectedResult = "Planet";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Field objectType = SolarSystemInformation.class.getDeclaredField("objectType");
        objectType.setAccessible(true);
        //Act
        setObjectType.invoke(cut, input);
        String actualResult = (String) objectType.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectType_is_stored_in_correct_format_Moon_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks moon objectType is successfully stored in correct format
        //Arrange
        String input = "Moon";
        String expectedResult = "Moon";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Field objectType = SolarSystemInformation.class.getDeclaredField("objectType");
        objectType.setAccessible(true);
        //Act
        setObjectType.invoke(cut, input);
        String actualResult = (String) objectType.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectType_is_stored_in_correct_format_Asteroid_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks asteroid objectType is successfully stored in correct format
        //Arrange
        String input = "Asteroid";
        String expectedResult = "Asteroid";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Field objectType = SolarSystemInformation.class.getDeclaredField("objectType");
        objectType.setAccessible(true);
        //Act
        setObjectType.invoke(cut, input);
        String actualResult = (String) objectType.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectType_setter_incorrect_format_throws_exception_stub() throws NoSuchMethodException {
        //Checks incorrectly formatted objectType throws exception
        //Arrange
        String input = "AB99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectType.invoke(cut, input));
    }

    @Test
    void test_ObjectType_setter_blank_throws_exception_stub() throws NoSuchMethodException {
        //Checks blank objectType (empty string) throws exception
        //Arrange
        String input = "";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectType.invoke(cut, input));
    }

    @Test
    void test_ObjectName_is_stored_in_correct_format_Earth_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks objectName is correctly formatted (Earth)
        //Arrange
        String input = "Earth";
        String expectedResult = "Earth";
        String objectType = "Planet";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        Field objectName = SolarSystemInformation.class.getDeclaredField("objectName");
        objectName.setAccessible(true);
        //Act
        setObjectType.invoke(cut, objectType);
        setObjectName.invoke(cut, input);
        String actualResult = (String) objectName.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectName_is_stored_in_correct_format_Asteroid_stub() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, NoSuchFieldException {
        //Checks objectName is correctly formatted (Asteroid)
        //Arrange
        String input = "99942 Apophis";
        String expectedResult = "99942 Apophis";
        String objectType = "Asteroid";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectType = SolarSystemInformation.class.getDeclaredMethod("setObjectType", String.class);
        setObjectType.setAccessible(true);
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        Field objectName = SolarSystemInformation.class.getDeclaredField("objectName");
        objectName.setAccessible(true);
        //Act
        setObjectType.invoke(cut, objectType);
        setObjectName.invoke(cut, input);
        String actualResult = (String) objectName.get(cut);
        //Assess
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_ObjectName_setter_incorrect_format_throws_exception_stub() throws NoSuchMethodException {
        //Checks incorrectly formatted objectName throws exception
        //Arrange
        String input = "AB99942Apo138M";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectName.invoke(cut, input));
    }

    @Test
    void test_ObjectName_setter_blank_throws_exception_stub() throws NoSuchMethodException {
        //Checks blank objectName (empty string) throws exception
        //Arrange
        String input = "";
        SolarSystemInformation cut = new SolarSystemInformation("AA0001","S0lar$ystem", new WebServiceStub());
        Method setObjectName = SolarSystemInformation.class.getDeclaredMethod("setObjectName", String.class);
        setObjectName.setAccessible(true);
        //Act and assert
        assertThrows(Exception.class, () -> setObjectName.invoke(cut, input));
    }

}
