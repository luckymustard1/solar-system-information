package com.qa.webservice.stub;

import com.qa.webservice.IWebService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class WebServiceStubTest {

    @Test
    void test_authentication() {
        //Checks the stub authenticates when username and password is passed to it
        //Arrange
        String userID = "userID";
        String password = "password";
        IWebService webService = new WebServiceStub();
        //Act
        boolean actualResult = webService.authenticate(userID, password);
        //Assert
        assertTrue(actualResult);
    }

    @Test
    void test_get_status_info_returns_CSV_Earth() {
        //Checks correct details are returned when getStatusInfo() is called for Earth
        //Arrange
        String input = "PEar150M";
        String expectedResult = "PEar150M,Planet,Earth,365,2440,57909050,330110000000000000000000";
        String userID = "userID";
        String password = "password";
        IWebService webService = new WebServiceStub();
        webService.authenticate(userID, password);
        //Act
        String actualResult = webService.getStatusInfo(input);
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_get_status_info_returns_CSV_Ceres() {
        //Checks correct details are returned when getStatusInfo() is called for Ceres
        //Arrange
        String input = "DCer416M";
        String expectedResult = "DCer416M,DwarfPlanet,Ceres,1683,470,414261000,938350000000000000000";
        String userID = "userID";
        String password = "password";
        IWebService webService = new WebServiceStub();
        webService.authenticate(userID, password);
        //Act
        String actualResult = webService.getStatusInfo(input);
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_get_status_info_returns_CSV_Phobos() {
        //Checks correct details are returned when getStatusInfo() is called for Phobos
        //Arrange
        String input = "MPho9T";
        String expectedResult = "MPho9T,Moon,Phobos,0,11,9376,10659000000000000";
        String userID = "userID";
        String password = "password";
        IWebService webService = new WebServiceStub();
        webService.authenticate(userID, password);
        //Act
        String actualResult = webService.getStatusInfo(input);
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_get_status_info_when_not_authenticated_fails() {
        //Checks no details (empty string) are returned when getStatusInfo() is called but
        //web service is not authenticated
        //Arrange
        String input = "PMer58M";
        String expectedResult = "";
        IWebService webService = new WebServiceStub();
        //Act
        String actualResult = webService.getStatusInfo(input);
        //Assert
        assertEquals(expectedResult, actualResult);
    }

    @Test
    void test_get_status_info_AOC_does_not_exist() {
        //Checks "No such classification or SMA code" is returned when getStatusInfo() is called
        //but no such AOC exists
        //Arrange
        String input = "YAhh10L";
        String expectedResult = "No such classification or SMA code";
        String userID = "userID";
        String password = "password";
        IWebService webService = new WebServiceStub();
        webService.authenticate(userID, password);
        //Act
        String actualResult = webService.getStatusInfo(input);
        //Assert
        assertEquals(expectedResult, actualResult);
    }
}